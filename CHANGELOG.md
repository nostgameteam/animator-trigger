# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.1] - 2021-01-05
### Changed
- Upgrade Collider Adapter package to 1.0.1

## [2.0.0] - 2021-01-04
### Changed
- Replace trigger-events package by collider-adapter
- Namespace from ActionCode.AnimatorTriggerModule to ActionCode.AnimatorTriggers

## [1.0.0] - 2020-08-01
### Added
- AnimatorTrigger component
- CHANGELOG
- README
- initial files
- Initial commit

[Unreleased]: https://bitbucket.org/nostgameteam/animator-trigger/branches/compare/2.0.1%0Dmaster
[2.0.1]: https://bitbucket.org/nostgameteam/animator-trigger/src/2.0.1/
[2.0.0]: https://bitbucket.org/nostgameteam/animator-trigger/src/2.0.0/
[1.0.0]: https://bitbucket.org/nostgameteam/animator-trigger/src/1.0.0/