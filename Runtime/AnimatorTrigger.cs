﻿using UnityEngine;
using ActionCode.ColliderAdapter;
using ActionCode.AnimationParameters;

namespace ActionCode.AnimatorTriggers
{
    /// <summary>
    /// Activates animator parameters when a target enters or leaves a trigger zone.
    /// <para>Works for 2D and 3D trigger colliders.</para>
    /// </summary>
	[DisallowMultipleComponent]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(TriggerActionDetector))]
    public sealed class AnimatorTrigger : MonoBehaviour
    {
        [SerializeField, Tooltip("The triggered animator.")]
        private Animator animator;
        [SerializeField, Tooltip("The Trigger Action Detector used to check triggers collisions.")]
        private TriggerActionDetector trigger;
        [SerializeField, Tooltip("Parameters triggered when enter in trigger zone.")]
        private ValueAnimatorParam[] onEnterParams = new ValueAnimatorParam[0];
        [SerializeField, Tooltip("Parameters triggered when exit out trigger zone.")]
        private ValueAnimatorParam[] onExitParams = new ValueAnimatorParam[0];

        private void Reset()
        {
            animator = GetComponent<Animator>();
            trigger = GetComponent<TriggerActionDetector>();
        }

        private void OnEnable()
        {
            trigger.OnEnter += HandleOnEnter;
            trigger.OnExit += HandleOnExit;
        }

        private void OnDisable()
        {
            trigger.OnEnter -= HandleOnEnter;
            trigger.OnExit -= HandleOnExit;
        }


        private void HandleOnEnter()
        {
            foreach (ValueAnimatorParam param in onEnterParams)
            {
                animator.SetParam(param);
            }
        }


        private void HandleOnExit()
        {
            foreach (ValueAnimatorParam param in onExitParams)
            {
                animator.SetParam(param);
            }
        }
    }
}