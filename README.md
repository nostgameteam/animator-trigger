# Animator Trigger

* Activates animator parameters when a target enters or leaves a trigger zone
* Unity minimum version: **2019.1**
* Current version: **2.0.1**
* License: **MIT**
* Dependencies: 
    - [com.actioncode.animation-parameters : 1.0.0](https://bitbucket.org/nostgameteam/animation-parameters/src/1.0.0/)
    - [com.actioncode.collider-adapter : 1.0.1](https://bitbucket.org/nostgameteam/com.actioncode.collider-adapter/src/1.0.1/)


## How To Use

Suppose you have a AnimationContoller and a Prefab setup as follow:

![animator-inspector](/Documentation~/animator-inspector.png)

This will produce this behavior on runtime:

![scene-showcase](/Documentation~/scene-showcase.gif)


## Installation

### Using the Package Registry Server

Follow the instructions inside [here](https://cutt.ly/ukvj1c8) and the package **ActionCode-Animator Trigger** will be available for you to install using the **Package Manager** windows.

### Using the Git URL

You will need a **Git client** installed on your computer with the Path variable already set. 

Use the **Package Manager** "Add package from git URL..." feature or add manually this line inside `dependencies` attribute: 

```json
"com.actioncode.animator-trigger":"https://bitbucket.org/nostgameteam/animator-trigger.git"
```

---

**Hyago Oliveira**

[BitBucket](https://bitbucket.org/HyagoGow/) -
[Unity Connect](https://connect.unity.com/u/hyago-oliveira) -
<hyagogow@gmail.com>