﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace ActionCode.AnimatorTriggers.Editor
{
    [CustomEditor(typeof(AnimatorTrigger))]
    public sealed class AnimatorTriggerEditor : UnityEditor.Editor
    {
        private SerializedProperty scriptProperty;
        private SerializedProperty animatorProperty;
        private SerializedProperty triggerProperty;
        private SerializedProperty onEnterParamsProperty;
        private SerializedProperty onExitParamsProperty;
        private ReorderableList onEnterReorderableList;
        private ReorderableList onExitReorderableList;

        private void OnEnable()
        {
            scriptProperty = serializedObject.FindProperty("m_Script");
            animatorProperty = serializedObject.FindProperty("animator");
            triggerProperty = serializedObject.FindProperty("trigger");
            onEnterParamsProperty = serializedObject.FindProperty("onEnterParams");
            onExitParamsProperty = serializedObject.FindProperty("onExitParams");

            // Why don't use onEnterParamsProperty.tooltip?
            // SerializedProperty.tooltip is empty even if set on component script (Unity BUG ISSUE ID: 885341)
            GUIContent onEnterLabel = new GUIContent(onEnterParamsProperty.displayName, "Parameters triggered when enter in trigger zone.");
            GUIContent onExitLabel = new GUIContent(onExitParamsProperty.displayName, "Parameters triggered when exit out trigger zone.");

            onEnterReorderableList = new ReorderableList(serializedObject, onEnterParamsProperty, true, true, true, true)
            {
                drawHeaderCallback = DrawHeaderTitle(onEnterLabel),
                drawElementCallback = DrawElement(onEnterParamsProperty)
            };

            onExitReorderableList = new ReorderableList(serializedObject, onExitParamsProperty, true, true, true, true)
            {
                drawHeaderCallback = DrawHeaderTitle(onExitLabel),
                drawElementCallback = DrawElement(onExitParamsProperty)
            };
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            GUI.enabled = false;
            EditorGUILayout.PropertyField(scriptProperty);
            GUI.enabled = true;
            EditorGUILayout.PropertyField(animatorProperty);
            EditorGUILayout.PropertyField(triggerProperty);
            EditorGUILayout.Space();

            onEnterReorderableList.DoLayoutList();
            onExitReorderableList.DoLayoutList();

            serializedObject.ApplyModifiedProperties();
        }

        private ReorderableList.HeaderCallbackDelegate DrawHeaderTitle(GUIContent label)
        {
            return (position) =>
            {
                EditorGUI.LabelField(position, label);
            };
        }

        private ReorderableList.ElementCallbackDelegate DrawElement(SerializedProperty property)
        {
            return (position, index, isActive, isFocused) =>
            {
                SerializedProperty element = property.GetArrayElementAtIndex(index);
                EditorGUI.PropertyField(position, element);
            };
        }
    }
}